<?php

namespace App\Http\Controllers\linebot\v2;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;

class WebhookController extends Controller
{
    //
    public function webhook(Request $request)
    {
        Log::info($request->all());
        return response('HTTP_OK', Response::HTTP_OK);
    }
}
